# Manufacturing Line Digital Twin

## Functions
- Data collection and processing from various sensors (machine status, production rate, etc.)
- Production process simulation
- Predictive maintenance for machines
- Optimization of production rate
- Visualization of real-time data

## Models
- Physical model of the manufacturing line
- Data model for sensor readings
- Behavioral model for production process
- Predictive model for machine maintenance
- Performance model for production optimization

## Tools
- Azure IoT for data collection
- R for data processing
- Scikit-learn for machine learning
- AnyLogic for simulation
- PowerBI for visualization

## Data
- Sensor data from the machines
- Operational data
- Historical maintenance records
- Production records
- Simulated data for various scenarios
