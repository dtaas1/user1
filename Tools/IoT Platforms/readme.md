Provide a foundation for connecting sensors, collecting data, etc. Examples include Azure IoT, AWS IoT, Google Cloud IoT, etc.
