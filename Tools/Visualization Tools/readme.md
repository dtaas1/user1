Used to create visual representations of the data and the digital twin. Examples include Tableau, PowerBI, D3.js, etc.
